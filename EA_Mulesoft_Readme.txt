EA Mulesoft Integration

The Mulesoft Einstein Analytics intergation piece is a solution to allowing real time updates to EA by using Mulesoft in conjunction with Salesforce Replay Channels.

The process involves three main components:
	1. A Salesforce change data capture stream.
	2. An Einstein Analytics DataSet to track selected data. 
	3. A Mulesoft flow.

Below is an indepth explanation of each part, and it's components. 

1 - A Salesforce chang data capture stream.
	Enter setup. 
	Search for Change Data Capture.
	Move any objects you wish to track from the left column to the right column to begin tracking.


2 - An Einstein Analytics Dataset.
	Enter the EA dashboard.
	Click All Items
	Click Datasets
	Click Create
	Select "From Salesforce"
	Create dataset


3 - The Mulesoft Flow
	The flow only needs to be run in order to perform the EA transfers in real time.
	If changes occur in the SF environment, or any of the requirements change the flow has been designed so that you can apply changes easily to via the configuration file.

	Settings:
		In Anypoint Studio or the IDE of your choice open up defaultSettings.yaml
		In this file there are several variables assigned values relating to the Salesforce environment.

		Salesforce
			userName: The username of the account to be used for all Einstein Analytics related functions 
			passWord: Password to the above user
			securityToken: The Salesforce Security Token related to the above user
			url: The URL to the Salesforce instance containing Einstein Analytics

			**channelPath: the channel path to a specific object. (NOTE: These usually follow the pattern of '/data/[objectName]ChangeEvent') (Second note, if the object is standard it will look like "AccountChangeEvent" however if it's custom it will look like "CustomAccount__ChangeEvent")

		Reporting
			email: The email address that error emails will be sent to

		DataSet
			**DataSet: The dataset name of a specific EA dataset.
